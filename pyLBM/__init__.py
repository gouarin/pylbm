# Authors:
#     Loic Gouarin <loic.gouarin@math.u-psud.fr>
#     Benjamin Graille <benjamin.graille@math.u-psud.fr>
#
# License: BSD 3 clause

from domain import Domain
from stencil import Stencil
from simulation import Simulation
import boundary as bc
import generator
from generator import *
from scheme import Scheme
from elements import *
from geometry import Geometry
import viewer
from vtk import VTKFile
