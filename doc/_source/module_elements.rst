the module elements
===================

.. automodule:: pyLBM.elements

.. warning::
  The geometrical elements are not yet implemented in 3D.
  Maybe in the next release...

The module elements contains all the geometrical shapes that can be used
to build the geometry.

The 2D elements are:

.. toctree::
  :maxdepth: 2

  The class Circle <class_circle>
  The class Parallelogram <class_parallelogram>
  The class Triangle <class_triangle>
