the module generator
------------------------------

.. automodule:: pyLBM.generator

.. autoclass:: pyLBM.generator.NumpyGenerator
   :members:
