the module stencil
======================

.. automodule:: pyLBM.stencil

.. toctree::
   :maxdepth: 2

   the class Stencil <class_stencil>
   the class OneStencil <class_onestencil>
   the class Velocity <class_velocity>
